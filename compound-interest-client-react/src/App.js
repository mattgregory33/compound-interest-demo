import React, { Component } from 'react';
import Axios from 'axios';
import './App.css';

class App extends Component {
    state = {answer: 0};

    setAnswer = (answer) => {
        this.setState((prevState) => ({
            answer: answer
        }));
    };

    render() {
        return (
            <div>
                <Form onSubmit={this.setAnswer}/>
                <Answer answer={this.state.answer}/>
            </div>
        );
    }
}

class Form extends Component {
    state = { principal: 0,
              rate: 0,
              frequency: 12,
              years: 1};

    handleSubmit = (event) => {
        event.preventDefault();
        Axios.get(`http://localhost:8080/compoundedInterest?p=${this.state.principal}&r=${this.state.rate}&cpy=${this.state.frequency}&yc=${this.state.years}`)
            .then(resp => {
                this.props.onSubmit(resp.data.futureValue);
            })
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <p><label for="principal" className="App-label">
                    Principal: </label><input id="principal" className="App-input"
                                     type="number"
                                     value={this.state.principal}
                                     onChange={(event) => this.setState({ principal: event.target.value })}/>
                </p>
                <p><label for="rate" className="App-label">
                    Rate: </label><input id="rate" className="App-input"
                                 type="number"
                                 value={this.state.rate}
                                 onChange={(event) => this.setState({ rate: event.target.value })}/>
                </p>
                <p><label for="frequency" className="App-label">
                    Compound Frequency: </label><select id="frequency"  className="App-input" onChange={(event) => this.setState({ frequency: event.target.value })}>
                        <option value="1">Yearly</option>
                        <option value="12" selected="selected">Monthly</option>
                        <option value="365">Daily</option>
                    </select>
                </p>
                <p><label for="years" className="App-label">Compound Years: </label><input id="years" className="App-input"
                                                          type="number"
                                                          value={this.state.years}
                                                          onChange={(event) => this.setState({ years: event.target.value })}/>
                </p>
                <button type="submit" className="App-button">Calculate</button>
            </form>
        )
    }
}

const Answer = (props) => {
    return (
        <div className="App-answer">${props.answer}</div>
    )
};

export default App;

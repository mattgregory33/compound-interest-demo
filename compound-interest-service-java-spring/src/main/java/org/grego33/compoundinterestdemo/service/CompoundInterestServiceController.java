package org.grego33.compoundinterestdemo.service;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class CompoundInterestServiceController {

    @CrossOrigin()
    @GetMapping("/compoundedInterest")
    public Map<Object, Object> computeCompoundInterest(
            @RequestParam("p") long principal,
            @RequestParam("r") short rate,
            @RequestParam("cpy") short compoundsPerYear,
            @RequestParam("yc") long yearsCompounded
    ) {
        CompoundInterestCalculator calculator = new CompoundInterestCalculator(principal, rate, compoundsPerYear, yearsCompounded);
        HashMap<Object, Object> response = new HashMap<Object, Object>(1);
        response.put("futureValue", calculator.calculate());
        return response;
    }

}

package org.grego33.compoundinterestdemo.service;

public class CompoundInterestCalculator {

    private long principal;
    private short rate;
    private short compoundsPerYear;
    private long yearsCompounded;

    public CompoundInterestCalculator(long principal, short rate, short compoundsPerYear, long yearsCompounded) {
        if (compoundsPerYear < 1) {
            throw new IllegalArgumentException("compoundsPerYear must be at least 1");
        }

        if (yearsCompounded < 1) {
            throw new IllegalArgumentException("yearsCompounded must be at least 1");
        }

        this.principal = principal;
        this.rate = rate;
        this.compoundsPerYear = compoundsPerYear;
        this.yearsCompounded = yearsCompounded;
    }

    public long calculate() {
        return Math.round(principal * Math.pow((1 + (rate/100.0) / compoundsPerYear),(compoundsPerYear * yearsCompounded)));
    }
}

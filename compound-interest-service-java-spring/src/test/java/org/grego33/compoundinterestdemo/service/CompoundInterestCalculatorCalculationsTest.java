package org.grego33.compoundinterestdemo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class CompoundInterestCalculatorCalculationsTest {

    private long principal;
    private short rate;
    private short compoundsPerYear;
    private long yearsCompounded;
    private long expected;

    public CompoundInterestCalculatorCalculationsTest(long principal, short rate, short compoundsPerYear, long yearsCompounded, long expected) {
        this.principal = principal;
        this.rate = rate;
        this.compoundsPerYear = compoundsPerYear;
        this.yearsCompounded = yearsCompounded;
        this.expected = expected;
    }

    @Parameterized.Parameters(name = "{index}: principal:{0}, rate:{1}, compoundsPerYear:{2}, yearsCompounded:{3}, expected:{4}")
    public static Collection<Object[]> testCases() {
        return Arrays.asList(new Object[][]{
                {1, (short)1, (short)12, 1, 1},
                {1, (short)20, (short)12, 100, 411435302},
                {5000, (short)5, (short)12, 10, 8235},
                {1000, (short)8, (short)1, 10, 2159},
                {1000, (short)8, (short)2, 10, 2191},
                {1000, (short)8, (short)4, 10, 2208},
                {1000, (short)8, (short)12, 10, 2220},
                {1000, (short)8, (short)365, 10, 2225}
        });
    }

    @Test
    public void testCompoundInterestCalculation() {
        CompoundInterestCalculator calculator = new CompoundInterestCalculator(principal, rate, compoundsPerYear, yearsCompounded);
        assertEquals(expected, calculator.calculate());
    }
}
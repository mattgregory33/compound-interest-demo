package org.grego33.compoundinterestdemo.service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CompoundInterestCalculatorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void mustCompoundAtLeastOncePerYear() throws IllegalArgumentException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("compoundsPerYear");
        new CompoundInterestCalculator(0, (short)0, (short)0, 1);
    }

    @Test
    public void mustCompoundAtLeastOneYear() throws IllegalArgumentException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("yearsCompounded");
        new CompoundInterestCalculator(0, (short)0, (short)1, 0);
    }
}